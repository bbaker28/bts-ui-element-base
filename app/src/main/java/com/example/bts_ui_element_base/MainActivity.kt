package com.example.bts_ui_element_base

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageButton
import android.widget.Switch
import android.widget.Toast
import com.google.android.material.chip.Chip
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {

    private var TAG: String = "MainActivity"
    private lateinit var button: Button
    private lateinit var switch: Switch
    private lateinit var imageButton: ImageButton
    private lateinit var floatingActionButton: FloatingActionButton
    private lateinit var chip: Chip
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button = findViewById(R.id.button)

        button.setOnClickListener { view ->
            Toast.makeText(this, "You have pressed the button!!!", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "You have pressed the button, here is the log!!")
        }

        switch = findViewById(R.id.switch1)
        switch.setOnCheckedChangeListener { buttonView, isChecked ->
            Toast.makeText(this, isChecked.toString(), Toast.LENGTH_SHORT).show()
        }

        imageButton = findViewById(R.id.imageButton)
        imageButton.setOnClickListener { view ->
            Toast.makeText(this, "This is your profile!", Toast.LENGTH_SHORT).show()
        }

        floatingActionButton = findViewById(R.id.floatingActionButton)
        floatingActionButton.setOnClickListener { view ->
            Toast.makeText(this, "Logging Out", Toast.LENGTH_SHORT).show()
        }

        chip = findViewById(R.id.chip2)
        chip.setOnClickListener { view ->
            Toast.makeText(this, "Mmmm tasty chips", Toast.LENGTH_SHORT).show()
        }
    }
}